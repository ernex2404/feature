import React, { Component } from 'react';
import Feature from './Components/Feature'
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      value: <Feature />,
      list: [],
      clicked: []
    };
    this.addEvent = this.addEvent.bind(this);
  }
  
  addEvent(event) {
    this.setState(state => {
      const list = state.list.concat(state.value);
      return {
        list,
        value: <Feature />,
      };
    });
  }

  onRemoveItem = i => {
    this.setState(state => {
      const list = state.list.filter((item, j) => i !== j);
      return {
        list,
      };
    });
  };

  render() {
    return (
      <div className="features">
      <h1>Add Feature to the Speech tree</h1>
      <button className="btn-primary"   onClick={ this.addEvent } >+ Add Feature</button>
      <button className="btn-primary" type="button" onClick={() => this.onRemoveItem(0)}> - Remove</button>
      <button className="btn-primary" >Description</button>
      {this.state.list.map(item => (
            <div key={item}>{item}</div>
          ))}
     </div>
    );
  }
}

export default App;
