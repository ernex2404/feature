import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Event from './Event';
export default class Context extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      value: <Event />,
      list: []
    };
    this.addEvent = this.addEvent.bind(this);
  }

  addEvent(event) {
    this.setState(state => {
      const list = state.list.concat(state.value);
      return {
        list,
        value: <Event />,
      };
    });
  }

  onRemoveItem = i => {
    this.setState(state => {
      const list = state.list.filter((item, j) => i !== j);
      return {
        list,
      };
    });
  };

  render() {
    return (
      this.state.length ? <div>day</div> : null,
      <div className="context">
        <h2>Hi I am a Context</h2>
        <button className="btn-primary"  onClick={ this.addEvent }>+ Add Event</button>
        <button className="btn-primary"  type="button" onClick={() => this.onRemoveItem(0)}>- Quit Event</button>
        {this.state.list.map(item => (
            <div key={item}>{item}</div>
          ))}
      </div>
    )
  }
}