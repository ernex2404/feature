import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Sentence from './Sentence';

export default class Event extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      value: <Sentence />,
      list: []
    };
    this.addEvent = this.addEvent.bind(this);
  }

  addEvent(event) {
    this.setState(state => {
      const list = state.list.concat(state.value);
      return {
        list,
        value: <Sentence />,
      };
    });
  }

  onRemoveItem = i => {
    this.setState(state => {
      const list = state.list.filter((item, j) => i !== j);
      return {
        list,
      };
    });
  };

  render() {
    return (
      <div className="event">
        <h3>Hi I am a Event</h3>
        <button className="btn-primary"  onClick={ this.addEvent }>+ Add Sentence</button>
        <button className="btn-primary"  type="button" onClick={() => this.onRemoveItem(0)}>- Quit Sentence</button>
        {this.state.list.map(item => (
            <div key={item}>{item}</div>
          ))}
      </div>
    )
  }
}