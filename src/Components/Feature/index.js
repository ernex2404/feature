import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import Context from './Context';

export default class Feature extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      value:  <Context />,
      list: []
    };
    this.addEvent = this.addEvent.bind(this);
  }

  addEvent(event) {
    this.setState(state => {
      const list = state.list.concat(state.value);
      return {
        list,
        value:  <Context />,
      };
    });
  }

  onRemoveItem = i => {
    this.setState(state => {
      const list = state.list.filter((item, j) => i !== j);
      return {
        list,
      };
    });
  };

  render() {
    return (
      <div className="feature">
        <h1>Hi I am a Feature</h1>
        <button className="btn-primary"  onClick={ this.addEvent }>+ Add Context</button>
        <button className="btn-primary"  type="button" onClick={() => this.onRemoveItem(0)}>- Quit Context</button>
        {this.state.list.map(item => (
            <div key={item}>{item}</div>
          ))}
      </div>
    )
  }
}
