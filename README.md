Coding Test

The aim of this exercise is to create a simple React & Redux application based on the following User Story. You should take no more than 3h to complete this task. In the end, you will be asked to explain the solution.
User Story
As a User, I need to be able to see all the Sentences of the LocCMS. LocCMS is the localization content manager system of EA. A Sentence is a structure used to generate dynamic content. Each sentence is categorized by Events, Contexts, and Features. This should be rendered as a tree, and let’s call it: Speech Tree. See how it should look like in the following example:

Feature 1
	Context 1.1 
	Context 2.2
		Event 2.2.1
			Sentence 2.2.1.1
Sentence 2.2.1.2
Event 2.2.2
Feature 2
Features & Requirements:
•	Features contain zero or more Contexts
•	Contexts contain zero or more Events
•	Events contain zero or more Sentences
•	The user should be able to add and remove Sentences to an Event
•	You should be able to click each structure to see a detail. 
Things to think about
•	What’s the UI & UX going to look like?
•	What happens if there are no features, contexts, events or sentences?
•	Thinking of the Redux structure of the Speech Tree not just for this use case. This feature is expected to grow.
•	How are going to store the list of Features, Contexts, Events, and Sentences? You are able to decide the structure of the response.
•	Do we need Unit Tests for this? It’s just a simple feature!

